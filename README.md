
# Simple VT100 System Monitor

Application C++ simple utilisée dans la séquence de mise à niveau C++/UML.

Elle affiche dans une interface textuelle des informations système (mémoire libre, charge système) en utilisant des séquences ANSI/VT100.

![alt text](img/screenshot.png  "Screenshot")

Cette application traduit le modèle UML suivant : 

![alt text](img/uml-model.png  "Modèle UML")

## Notions UML abordées

* membres publics/privés

* relations de composition et utilisation

* méthodes statiques

* rôle UML

## Autres notions abordées

Outre les notions UML, l'application illustre :

* l'appel d'un programme externe depuis le code C++ avec ``fork()/exec()`` ou ``popen()``

* la notion de *pipe* au niveau du shell

## Compilation

``g++ -o sysmonitor main.cpp app.cpp sysinfo.cpp ansiterm.cpp``

NOTE : Pour simuler une augmentation de charge système, on pourra sous *Linux* utiliser à plusieurs reprises la commande suivante : ``yes > /dev/null &``
Pour revenir à la normale, saisir : ``sudo killall yes``.