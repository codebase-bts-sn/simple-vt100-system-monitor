#pragma once

#include <string>

using namespace std;

class AnsiTerm {
    public :
    typedef enum : int {
        BLACK = 30
        , RED = 31
        , GREEN = 32
        , YELLOW = 33
        , BLUE = 34
        , MAGENTA = 35
        , CYAN = 36
        , WHITE = 37
        , DEFAULT = 39
    } fgcolor_t; // foreground colors
    void print(string msg);
    void print(string msg, fgcolor_t color);
    void print(int row, int col, string msg);
    void print(int row, int col, string msg, fgcolor_t color);
    void clearScreen();
    void eraseLine();
    void eraseEndOfLine();
    void setFgColor(fgcolor_t color);
    void enableCursor(bool onOff);
    void setCursorPosition(int row, int col);
    bool kbhit();
    void enableCanonicalMode(bool on);
};