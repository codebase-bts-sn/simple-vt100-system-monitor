//#define POPEN
#define FORK_EXEC

#ifdef FORK_EXEC
#include <unistd.h>
#include <sys/wait.h>
#endif

#include <iostream>
#include <ctime>

using namespace std;

#include "sysinfo.h"

string SysInfo::getIsoDate() {
    time_t now;
    time(&now);
    char buf[sizeof "yyyy-mm-ddThh:mm:ssZ"];
    strftime(buf, sizeof buf, "%Y-%m-%dT%H:%M:%SZ", gmtime(&now));
    string isoDate(buf);

    return isoDate;
}

/**
 * Retourne la quantité de mémoire libre restante
 * 
 * Sous Windows : (Get-CIMInstance Win32_OperatingSystem | Select FreePhysicalMemory | Format-Table -HideTableHeaders | Out-String).trim() 
 */
unsigned long SysInfo::getFreeMem() {
    string freeMemStr = getStdoutFromChildProcess("/usr/bin/cat /proc/meminfo | /usr/bin/sed -n '2p' | /usr/bin/grep -P '\\d+' -o");

    return stoi(freeMemStr);
}

/**
 * Retourne la charge moyenne du processeur
 * Voir https://fr.wikipedia.org/wiki/Load_average
 * 
 * Sous Windows : ((get-counter -counter  "\Système\Longueur de la file du processeur").
 * counterSamples[0] | Select CookedValue | Format-Table -HideTableHeaders | Out-String).trim()
 */
float SysInfo::getLoadAverage() {
    string loadAvgStr = getStdoutFromChildProcess("/usr/bin/uptime | grep -P '(?<=average: )[\\d.]+' -o");
    return stof(loadAvgStr);
}

/**
 * Retourne le nombre de coeurs du processeur
 * 
 * Sous Windows : Get-WmiObject -class Win32_processor | ft NumberOfCores -HideTableHeaders | Out-String).trim()
 */
int SysInfo::getNbCores() {
    string nbCores = getStdoutFromChildProcess("/usr/bin/cat /proc/cpuinfo| /usr/bin/grep 'processor' -c");

    return stoi(nbCores);
}

string SysInfo::getStdoutFromChildProcess(string cmd) {
#ifdef POPEN    
    string data;
    FILE *stream;
    const int max_buffer = 256;
    char buffer[max_buffer];
    cmd.append(" 2>&1");

    stream = popen(cmd.c_str(), "r");

    if (stream)
    {
        while (!feof(stream)) {
            if (fgets(buffer, max_buffer, stream) != NULL) {
                data.append(buffer);
            }
        }
        pclose(stream);
    }
    return data;    
#endif

#ifdef FORK_EXEC 
    string cmdOutput = "";
    // On crée un pipe dont les descripteurs sont stockés dans le tableau `fds`
    int fds[2];
    pipe(fds);
    if(pipe(fds) == -1) {
        cerr << "erreur pipe()" << endl;
        exit(EXIT_FAILURE);
    }

    // On forke le processus
    pid_t pid = fork();
  
    switch(pid) {
        case -1 : // Erreur fork
            cerr << "erreur fork()" << endl;
            exit(EXIT_FAILURE);
            break;

        case 0 : // On est dans le fils
            // On ferme l'extrémité de lecture du pipe car inutilisée
            if (close(fds[ 0 ]) == -1) {
                cerr << "close(fds[0] fils" << endl;
                exit(EXIT_FAILURE);
            }    

            if( fds[ 1 ] != STDOUT_FILENO) {    
                // On clone le descripteur d'écriture du pipe dans stdout (après avoir fermé ce dernier)
                // (=> la sortie standard sera donc désormais redirigée dans le descripteur contenu dans fds[1])
                dup2(fds[1], STDOUT_FILENO);
                // On ferme les descripteurs contenus dans fds[] car ils ne sont plus utiles
                // (rappel : stdout est une copie de fds[1] après l'appel précédent à dup2())
                close(fds[0]);
                close(fds[1]);
                // On exécute la commande
                if (execl("/usr/bin/sh", (char *)"sh", (char *)"-c", cmd.c_str(), NULL) < 0) {
                    cerr << "Erreur d'exécution de la commande `/usr/bin bash -c " << cmd << "`" << endl;
                    exit(EXIT_FAILURE);
                } else {
                    exit(EXIT_SUCCESS);
                }
            }
            break;

        default : // On est dans le parent
            // On sauvegarde le descripteur d'entrée standard d'origine (clavier)
            int defaultStdin = dup(STDOUT_FILENO);
            // On clone le descripteur de lecture du pipe dans stdin (après avoir fermé ce dernier)
            // (=> l'entrée standard proviendra donc désormais du descripteur contenu dans fds[0])
            if( fds[ 0 ] != STDIN_FILENO) {    
                dup2(fds[ 0 ], STDIN_FILENO);    
                // On ferme les descripteurs contenus dans fds[] car ils ne sont plus utiles
                // (rappel : stdin est une copie de fds[0] après l'appel précédent à dup2())
                close(fds[ 0 ]);          
                close(fds[ 1 ]);

                // On lit la chaine retournée par le processus fils
                cin >> cmdOutput;
            }
            // On restitue le descipteur d'entrée standard d'origine (clavier)
            dup2(defaultStdin, STDIN_FILENO);

            /* On attend la fin d'exécution du fils */
            int status;
            pid_t wpid = wait(&status);

            if (wpid == -1) {
                cerr << "erreur wait()" << endl;
                exit(EXIT_FAILURE);
            } else {
                // On teste le code de sortie du processus fils
                if ( !WIFEXITED(status) ) {
                    cmdOutput = "";
                }
            }
            break;
    }

    return cmdOutput;

#endif
}