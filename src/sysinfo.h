#pragma once

#include <string>

using namespace std;

class SysInfo {
    public :
    static unsigned long getFreeMem();
    static float getLoadAverage();
    static int getNbCores();   
    static string getIsoDate();
    private:
    static string getStdoutFromChildProcess(string cmd);
};