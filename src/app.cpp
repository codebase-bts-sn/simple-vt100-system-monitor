#include <iostream> 
#include <sstream> 
#include <thread>
#include <vector>

#include "app.h"
#include "sysinfo.h"

using namespace std;

void App::exec() {
     _term.enableCanonicalMode(false);

     _term.clearScreen();

     _term.enableCursor(false);

     _term.setFgColor(AnsiTerm::DEFAULT);
     _term.print(1, 1, "Heure          :");
     _term.print(2, 1, "Mémoire libre  :");
     _term.print(3, 1, "Charge moyenne :");
     _term.print(5, 1, "<Appuyer sur 'q' pour quitter>");

     bool quit = false;

     int unitIdx = 0;
     vector<pair<double,string>> memUnit{{1e0," kB"}, {1e3, " MB"}, {1e6, " GB"}};

     int nbCores = SysInfo::getNbCores();

     while ( !quit ) {
          while ( !_term.kbhit() ) {          
               _term.print(1, 18, SysInfo::getIsoDate());

               double freeMem = static_cast<double>(SysInfo::getFreeMem()) / memUnit.at(unitIdx).first;
               stringstream metric;
               metric << freeMem << memUnit.at(unitIdx).second;
               _term.setCursorPosition(2, 18);
               _term.eraseEndOfLine();
               _term.print(metric.str());

               float loadAvg = SysInfo::getLoadAverage();
               if(loadAvg <= (0.75 *nbCores)) {
                    _term.setFgColor(AnsiTerm::GREEN);
                } else if ( (loadAvg > (0.75 * nbCores)) && (loadAvg < nbCores)) {
                    _term.setFgColor(AnsiTerm::MAGENTA);
                } else {
                    _term.setFgColor(AnsiTerm::RED);
                }

               _term.setCursorPosition(3, 18);
               _term.eraseEndOfLine();
               _term.print(to_string(loadAvg));
               _term.setFgColor(AnsiTerm::DEFAULT);

               this_thread::sleep_for(1000ms);               
          }

          int c = getchar();
          switch (c) {
               case 'u':
                    unitIdx = (unitIdx + 1) % 3;
                    break;
               case 'q':
               case 'Q':
                    quit = true;
                    break;
               default :
                    break;
          }  

     }

     _term.enableCanonicalMode(true);

     _term.enableCursor(true);

     _term.setCursorPosition(5, 1);

     _term.eraseLine();

     _term.print("Bye !\n");

}