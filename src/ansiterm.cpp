#include <fcntl.h>
#include <termios.h>
#include <sys/ioctl.h>

#include <iostream>

#include "ansiterm.h"

void AnsiTerm::clearScreen() {
    cout << "\x1b[2J";
    cout.flush();
}

void AnsiTerm::eraseLine() {
    cout << "\x1b[2K";
    cout.flush();
}

void AnsiTerm::eraseEndOfLine() {
    cout << "\x1b[K";
    cout.flush();
}

void AnsiTerm::enableCursor(bool on) {
    cout << (on ? "\x1b[?25h" : "\x1b[?25l");
    cout.flush();
}

void AnsiTerm::setFgColor(fgcolor_t color) {
    cout << "\x1b[" << color << "m";
    cout.flush();
}

void AnsiTerm::setCursorPosition(int row, int col) {
    cout << "\x1b[" << row << ";" << col << "H";
    cout.flush();
}

void AnsiTerm::print(string msg) {
    cout << msg;
    cout.flush();
}

void AnsiTerm::print(string msg, fgcolor_t color) {
    cout << "\x1b[" << color << "m" << msg << "\x1b[" << DEFAULT;
    cout.flush();
}

void AnsiTerm::print(int row, int col, string msg) {
    cout << "\x1b[" << row << ";" << col << "H" << msg;
    cout.flush();
}

void AnsiTerm::print(int row, int col, string msg, fgcolor_t color) {
    cout << "\x1b[" << row << ";" << col << "H\x1b[" << color << "m" << msg << "\x1b[" << DEFAULT << "m";
    cout.flush();
}

/**
 * Active/Désactive le mode canonique du terminal
 * (i.e. interprétation par ligne (-> mode canonique) plutôt que par caractère (mode non canonique)
 */
void AnsiTerm::enableCanonicalMode(bool on)
{
  termios term;
  tcgetattr(0, &term);
  if(on) {
    term.c_lflag |= ICANON | ECHO;
  } else {
    term.c_lflag &= ~(ICANON | ECHO);
  }
  tcsetattr(0, TCSANOW, &term);
}

/**
 * Indique si une touche clavier a été enfoncée
 */
bool AnsiTerm::kbhit()
{
  int bytesWaiting;
  ioctl(0, FIONREAD, &bytesWaiting);
  return (bytesWaiting > 0);
}