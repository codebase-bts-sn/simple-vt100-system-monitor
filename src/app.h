#pragma once

#include "ansiterm.h"

class App {
    public :
    void exec();

    private:
        AnsiTerm _term;
};